<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/ws/districts', 'HomeController@districts')->name('home.districts');
Route::get('/ws/neighborhoods', 'HomeController@neighborhoods')->name('home.neighborhoods');

Route::group([
    'namespace' => 'Manager',
    'as' => 'manager.',
    'prefix' => 'manager',
    'middleware' => ['auth', 'auth.as.manager'],
], function () {
    Route::resource('farmers', 'FarmerController');
    Route::get('/farms/create-for-farmer/{farmer_id}', 'FarmController@createForOneFarmer')->name('farms.farmer.create');
    Route::post('/farms/store-for-farmer/{farmer_id}', 'FarmController@storeForOneFarmer')->name('farms.farmer.store');
    Route::get('/farm-maps/add/{id}', 'FarmMapController@add')->name('farm-maps.add');
    Route::post('/farm-maps/store/{id}', 'FarmMapController@store')->name('farm-maps.store');
    Route::resource('users', 'UserController');

});

Route::group([
    'namespace' => 'Agent',
    'as' => 'agent.',
    'prefix' => 'agent',
    'middleware' => ['auth', 'auth.as.agent'],
], function () {
    Route::resource('farmers', 'FarmerController');
    Route::get('/farms/create-for-farmer/{farmer_id}', 'FarmController@createForOneFarmer')->name('farms.farmer.create');
    Route::post('/farms/store-for-farmer/{farmer_id}', 'FarmController@storeForOneFarmer')->name('farms.farmer.store');
    Route::get('/farm-maps/add/{id}', 'FarmMapController@add')->name('farm-maps.add');
    Route::post('/farm-maps/store/{id}', 'FarmMapController@store')->name('farm-maps.store');

});

Route::group([
    'namespace' => 'Farmer',
    'as' => 'farmer.',
    'prefix' => 'farmer',
    'middleware' => ['auth', 'auth.as.farmer'],
], function () {
    Route::resource('farmers', 'FarmerController');
    Route::get('/farms/create-for-farmer/{farmer_id}', 'FarmController@createForOneFarmer')->name('farms.farmer.create');
    Route::post('/farms/store-for-farmer/{farmer_id}', 'FarmController@storeForOneFarmer')->name('farms.farmer.store');
    Route::get('/farm-maps/add/{id}', 'FarmMapController@add')->name('farm-maps.add');
    Route::post('/farm-maps/store/{id}', 'FarmMapController@store')->name('farm-maps.store');

});


Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/search', 'HomeController@search')->name('search')->middleware('auth');






Auth::routes();