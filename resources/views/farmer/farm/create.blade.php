@extends('layouts.app') 

@section('content')
     @include('farmer.farm.shared.form', ['url' => ['farmer.farms.farmer.store', $farmer->id], 'method' => 'post'])
@endsection