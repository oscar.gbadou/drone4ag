{!! Form::model($farmMap, ['route' => $url, 'method' => $method, 'files' => true]) !!}
<h4 style="text-align: center">Ajouter une carte</h4>
<br>
<div class="row">
  <div class="col-sm-12">
    <div class="form-group {{ $errors->has('comment') ? 'has-danger' : '' }}">
      {!! Form::label('comment', 'Ajouter un commentaire', ['class' => 'control-label']) !!}
      {!! Form::textarea('comment', old('comment'), ['rows' => '4', 'placeholder' => 'Ajouter un commentaire', 'required', 'class' => 'form-control ' . ($errors->has('comment') ? 'form-control-danger' : '')]) !!}
      @if ($errors->has('comment'))
      <div class="form-control-feedback">
        {{ $errors->first('comment') }}
      </div>
      @endif
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <label for="">Ajouter une image de la carte</label>
    <div class="custom-file">
      <input name="farm_map" type="file" class="custom-file-input" id="customFile" accept="image/*" onchange="preview_image(event)">
      <label class="custom-file-label" for="customFile">Choisir ou prendre photo</label>
    </div>
    <p style="text-align: center; margin: 10px">
      <img class="img-thumbnail" id="output_image" />
    </p>
  </div>
</div>
<br><br>
<p style="text-align: center">
   {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
</p>
{!! Form::close() !!}
