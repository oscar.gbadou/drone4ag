@extends('layouts.app') 

@section('content')
     @include('farmer.farm_map.shared.form', ['url' => ['farmer.farm-maps.store', $farm->id], 'method' => 'post'])
@endsection