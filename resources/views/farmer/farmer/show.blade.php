@extends('layouts.app') @section('content')
<h4 style="text-align: center">Presentation du producteur
    <b>{{ $farmer->first_name }} {{ $farmer->last_name }}</b>
</h4>
<br>
<div class="row">
    <div class="col-12">

        <p>
            <img src="{{ asset($farmer->photo()) }}" alt="" class="img-thumbnail" style="max-height: 250px; float: left; margin-right: 10px">
            <b>Nom:</b> {{ $farmer->last_name }}
            <br>
            <b>Prenom:</b> {{ $farmer->first_name }}
            <br>
            <b>Commune:</b> {{ $farmer->town->name }}
            <br>
            <b>Arrondissement:</b> {{ $farmer->district->name }}
            <br>
            <b>Village:</b> {{ $farmer->neighborhood->name }}
            <br>
            <b>Detail:</b> {{ $farmer->detail }}
            <br>
            <b>Code du menage:</b> {{ $farmer->household_code }}
            <br>
            <b>Code du repondant:</b> {{ $farmer->responder_code }}
            <br>
            <b>Nom du chef de menage:</b> {{ $farmer->household_head_name }}
            <br>
            <b>Telephone:</b> {{ $farmer->phone }}
            <br>
            <b>Natif de la localite?</b> @if($farmer->locality_native) Oui @else Non @endif
            <br>
            <b>Type de menage:</b> {{ $farmer->household_type }}
            <br>
            <b>Nombre de riziculteurs dans le menage:</b> {{ $farmer->rice_farmer_number }}
        </p>
    </div>
    <div class="">

    </div>
</div>
<br>
<p class="text-secondary">Les champs de
    <b>{{ $farmer->first_name }} {{ $farmer->last_name }}</b>
</p>
@if(Auth::user()->role == 'ROLE_FARMER' && $farmer->user_id == Auth::user()->id)
<p>
    <a href="{{ route('farmer.farms.farmer.create', ['farmer_id' => $farmer->id]) }}" class="btn btn-outline-danger">
        <i class="fas fa-plus"></i> Ajouter un champ</a>
</p>
@endif
@if(count($farmer->farms) > 0)
<div class="row">
    @foreach($farmer->farms as $farm)
    <div class="col-sm-12 col-md-4" style="margin-bottom: 10px">
        <div class="card">
            @if(count($farm->farmMaps) > 0)
            <img style="height: 200px;" class="card-img-top" src="{{ asset($farm->farmMaps[0]->map()) }}"> @endif
            <div class="card-body">
                <h5 class="card-title">Champ de: {{ $farm->cultivation_practiced }}</h5>
                <p class="card-text">{{ $farm->area }} hectares</p>
                <a data-toggle="modal" data-target="#farm-{{$farm->id}}-modal" href="#" class="btn btn-primary">
                    <i class="fa fa-eye"></i> Consulter</a>
                @if(Auth::user()->role == 'ROLE_FARMER' && $farmer->user_id == Auth::user()->id)
                <a href="{{ route('farmer.farm-maps.add', ['id' => $farm->id]) }}" class="btn btn-dark">
                    <i class="fa fa-map"></i> Ajouter une carte</a>
                @endif
                <div class="modal fade" id="farm-{{$farm->id}}-modal" tabindex="-1" role="dialog" aria-labelledby="farm-{{$farm->id}}-modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="farm-{{$farm->id}}-modalLabel">Les cartes du champ de: {{ $farm->cultivation_practiced }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @foreach($farm->farmMaps as $farmMap)
                                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="@if($loop->first) active @endif"></li>
                                    @endforeach
                                </ol>
                                <div class="carousel-inner">
                                    @foreach($farm->farmMaps as $farmMap)
                                    <div class="carousel-item @if ($loop->first) active @endif">
                                        <img style="height: 400px;" class="d-block w-100" src="{{ asset($farmMap->map()) }}">
                                        <div class="carousel-caption d-none d-md-block">
                                            <div style="background: rgba(0,0,0,0.5); color: white; border-radius: 5px">
                                                <p>
                                                    @if($farmMap->comment && $farmMap->comment != '')
                                                     {{ $farmMap->comment }} <br>
                                                    @endif
                                                    {{ $farmMap->created_at }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@else
<p>Aucun champ enregistrer pour ce producteur.</p>
@endif @endsection