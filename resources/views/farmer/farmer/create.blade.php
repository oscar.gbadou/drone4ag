@extends('layouts.app') 

@section('content')
     @include('farmer.farmer.shared.form', ['url' => ['farmer.farmers.store', $farmer], 'method' => 'post'])
@endsection