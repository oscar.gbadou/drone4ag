<nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home' )}}">Drone App</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                
            </ul>
            <ul class="navbar-nav">
                @if(Auth::user())

                @if(Auth::user()->role == 'ROLE_MANAGER')
                    @include('shared.header.manager')
                @endif

                @if(Auth::user()->role == 'ROLE_AGENT')
                     @include('shared.header.agent')
                @endif

                @if(Auth::user()->role == 'ROLE_FARMER')
                     @include('shared.header.farmer')
                @endif
                
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-tie"></i> {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
                @else
                <li class="nav-item active">
                    <a href="{{ route('register') }}" class="nav-link">
                        <i class="fas fa-user-plus"></i> Inscription
                    </a>
                </li>
                <li class="nav-item active">
                    <a href="{{ route('login') }}" class="nav-link">
                        <i class="fas fa-user-lock"></i> Connexion
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>