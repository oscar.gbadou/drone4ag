@if (session()->has('success'))
<div class="alert alert-success" role="alert">
  <strong>Opération réussie !</strong> {{ session()->get('success') }}
</div>
@endif

@if (session()->has('status'))
<div class="alert alert-info" role="alert">
  <strong>Status !</strong> {{ session()->get('status') }}
</div>
@endif

@if (session()->has('error'))
<div class="alert alert-danger" role="alert">
  <strong>Opération échouée !</strong> {{ session()->get('error') }}
</div>
@endif
