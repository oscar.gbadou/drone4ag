<div class="row">
    <div class="col">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            Filtrer
        </a>
    </div>
</div>
<br>
<div class="collapse" id="collapseExample">
     {!! Form::open(['url' => '/search', 'method' => 'get']) !!}
    <div class="card card-body">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="form-group {{ $errors->has('town_id') ? 'has-danger' : '' }}">
                    {!! Form::label('town_id', 'Commune', ['class' => 'control-label']) !!} {!! Form::select('town_id', $towns, null, ['data-style'
                    => 'btn-outline-secondary', 'title' => 'Choisissez votre commune', 'data-live-search' => 'true',
                    'class' => 'selectpicker form-control ' . ($errors->has('town_id') ? 'form-control-danger' : '')]) !!} @if ($errors->has('town_id'))
                    <div class="form-control-feedback">
                        {{ $errors->first('town_id') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="form-group {{ $errors->has('district_id') ? 'has-danger' : '' }}">
                    {!! Form::label('district_id', 'Arrondissement', ['class' => 'control-label']) !!} {!! Form::select('district_id', [], null,
                    ['placeholder' => 'Choisissez un arrondissement', 'class' => 'form-control' . ($errors->has('district_id')
                    ? 'form-control-danger' : '')]) !!} @if ($errors->has('district_id'))
                    <div class="form-control-feedback">
                        {{ $errors->first('district_id') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="form-group {{ $errors->has('neighborhood_id') ? 'has-danger' : '' }}">
                    {!! Form::label('neighborhood_id', 'Village/Quartier', ['class' => 'control-label']) !!} {!! Form::select('neighborhood_id',
                    [], null, ['placeholder' => 'Choisissez un village/quartier', 'class' => 'form-control' . ($errors->has('neighborhood_id')
                    ? 'form-control-danger' : '')]) !!} @if ($errors->has('neighborhood_id'))
                    <div class="form-control-feedback">
                        {{ $errors->first('neighborhood_id') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label for="">Culture</label>
                <input type="search" name="culture" class="form-control" placeholder="Culture">
            </div>
        </div>
        <br>
        <p style="text-align: center">
            <button type="submit" class="btn btn-success">Rechercher</button>
        </p>
    </div>
     {!! Form::close() !!}
</div>

<br>