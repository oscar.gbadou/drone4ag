<li class="nav-item active">
    <a href="{{ route('manager.farmers.index') }}" class="nav-link">
        <i class="fas fa-list-alt"></i> Producteurs
    </a>
</li>
<li class="nav-item active">
    <a href="{{ route('manager.users.index') }}" class="nav-link">
        <i class="fas fa-users-cog"></i> Utilisateurs
    </a>
</li>