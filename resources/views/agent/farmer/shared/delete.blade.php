{!! Form::model($farmer, ['route' => $url, 'method' => $method, 'style' => 'display: inline']) !!}
<button title="Supprimer" type="submit" class="btn btn-danger btn-sm">
    Supprimer
</button>
{!! Form::close() !!}
