{!! Form::model($farmer, ['route' => $url, 'method' => $method, 'files' => true]) !!}
<h4>Ajout d'un producteur</h4>
<br>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="form-group {{ $errors->has('first_name') ? 'has-danger' : '' }}">
            {!! Form::label('first_name', 'Nom', ['class' => 'control-label']) !!} {!! Form::text('first_name', old('first_name'), ['placeholder'
            => 'Nom du producteur', 'required', 'class' => 'form-control ' . ($errors->has('first_name') ? 'form-control-danger'
            : '')]) !!} @if ($errors->has('first_name'))
            <div class="form-control-feedback">
                {{ $errors->first('first_name') }}
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="form-group {{ $errors->has('last_name') ? 'has-danger' : '' }}">
            {!! Form::label('last_name', 'Prenoms', ['class' => 'control-label']) !!} {!! Form::text('last_name', old('last_name'), ['placeholder'
            => 'Prenoms du producteur', 'required', 'class' => 'form-control ' . ($errors->has('last_name') ? 'form-control-danger'
            : '')]) !!} @if ($errors->has('last_name'))
            <div class="form-control-feedback">
                {{ $errors->first('last_name') }}
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-group {{ $errors->has('household_head_name') ? 'has-danger' : '' }}">
            {!! Form::label('household_head_name', 'Nom du chef de menage', ['class' => 'control-label']) !!} {!! Form::text('household_head_name',
            old('household_head_name'), ['placeholder' => 'Le nom du chef de menage', 'class' => 'form-control ' . ($errors->has('household_head_name')
            ? 'form-control-danger' : '')]) !!} @if ($errors->has('household_head_name'))
            <div class="form-control-feedback">
                {{ $errors->first('household_head_name') }}
            </div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="form-group {{ $errors->has('town_id') ? 'has-danger' : '' }}">
            {!! Form::label('town_id', 'Commune', ['class' => 'control-label']) !!} {!! Form::select('town_id', $towns, null, ['data-style'
            => 'btn-outline-secondary', 'title' => 'Choisissez votre commune', 'required', 'data-live-search' => 'true',
            'class' => 'selectpicker form-control ' . ($errors->has('town_id') ? 'form-control-danger' : '')]) !!} @if ($errors->has('town_id'))
            <div class="form-control-feedback">
                {{ $errors->first('town_id') }}
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="form-group {{ $errors->has('district_id') ? 'has-danger' : '' }}">
            {!! Form::label('district_id', 'Arrondissement', ['class' => 'control-label']) !!} {!! Form::select('district_id', [$farmer->district_id
            => $farmer->district->name], null, ['placeholder' => 'Choisissez un arrondissement', 'required', 'class' => 'form-control'
            . ($errors->has('district_id') ? 'form-control-danger' : '')]) !!} @if ($errors->has('district_id'))
            <div class="form-control-feedback">
                {{ $errors->first('district_id') }}
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="form-group {{ $errors->has('neighborhood_id') ? 'has-danger' : '' }}">
            {!! Form::label('neighborhood_id', 'Village/Quartier', ['class' => 'control-label']) !!} {!! Form::select('neighborhood_id',
            [$farmer->neighborhood_id => $farmer->neighborhood->name], null, ['placeholder' => 'Choisissez un village/quartier', 'required', 'class' => 'form-control' . ($errors->has('neighborhood_id')
            ? 'form-control-danger' : '')]) !!} @if ($errors->has('neighborhood_id'))
            <div class="form-control-feedback">
                {{ $errors->first('neighborhood_id') }}
            </div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-group {{ $errors->has('household_code') ? 'has-danger' : '' }}">
            {!! Form::label('household_code', 'Code du menage', ['class' => 'control-label']) !!} {!! Form::text('household_code', old('household_code'),
            ['placeholder' => 'Le code du menage', 'class' => 'form-control ' . ($errors->has('household_code') ? 'form-control-danger'
            : '')]) !!} @if ($errors->has('household_code'))
            <div class="form-control-feedback">
                {{ $errors->first('household_code') }}
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-group {{ $errors->has('responder_code') ? 'has-danger' : '' }}">
            {!! Form::label('responder_code', 'Code du repondant', ['class' => 'control-label']) !!} {!! Form::text('responder_code',
            old('responder_code'), ['placeholder' => 'Le code du repondant', 'class' => 'form-control ' . ($errors->has('responder_code')
            ? 'form-control-danger' : '')]) !!} @if ($errors->has('responder_code'))
            <div class="form-control-feedback">
                {{ $errors->first('responder_code') }}
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-group {{ $errors->has('rice_farmer_number') ? 'has-danger' : '' }}">
            {!! Form::label('rice_farmer_number', 'Nombre de riziculteurs dans votre ménage', ['class' => 'control-label']) !!} {!! Form::number('rice_farmer_number',
            old('rice_farmer_number'), ['placeholder' => 'Nombre de riziculteurs dans votre ménage', 'class' => 'form-control
            ' . ($errors->has('rice_farmer_number') ? 'form-control-danger' : '')]) !!} @if ($errors->has('rice_farmer_number'))
            <div class="form-control-feedback">
                {{ $errors->first('rice_farmer_number') }}
            </div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-group {{ $errors->has('phone') ? 'has-danger' : '' }}">
            {!! Form::label('phone', 'Numéro de téléphone ou d’un contact voisin', ['class' => 'control-label']) !!} {!! Form::text('phone',
            old('phone'), ['placeholder' => 'Numéro de téléphone ou d’un contact voisin', 'class' => 'form-control ' . ($errors->has('phone')
            ? 'form-control-danger' : '')]) !!} @if ($errors->has('phone'))
            <div class="form-control-feedback">
                {{ $errors->first('phone') }}
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-group {{ $errors->has('locality_native') ? 'has-danger' : '' }}">
            {!! Form::label('locality_native', 'Etes-vous natif de la localité ou migrant?', ['class' => 'control-label']) !!}
            <br> {!! Form::checkbox('locality_native', 'value', old('locality_native'), []) !!} @if ($errors->has('locality_native'))
            <div class="form-control-feedback">
                {{ $errors->first('locality_native') }}
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="form-group {{ $errors->has('household_type') ? 'has-danger' : '' }}">
            {!! Form::label('household_type', 'Type de menage', ['class' => 'control-label']) !!} {!! Form::select('household_type',
            ['1' => 'Mari et épouse présents', '2' => 'Homme chef avec une autre femme adulte', '3' => 'Femme chef avec un
            autre homme adulte', '4' => 'Homme ou femme seul sans autre adulte de sexe opposé', '5' => 'Autres cas'], null,
            ['placeholder' => 'Choisissez un type', 'class' => 'form-control'. ($errors->has('household_type') ? 'form-control-danger'
            : '')]) !!} @if ($errors->has('household_type'))
            <div class="form-control-feedback">
                {{ $errors->first('household_type') }}
            </div>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-12">
        <label for="">Ajouter une photo du producteur</label>
        <div class="custom-file">
            <input name="photo" type="file" class="custom-file-input" id="customFile" accept="image/*" onchange="preview_image(event)">
            <label class="custom-file-label" for="customFile">Choisir ou prendre photo</label>
        </div>
        <p style="text-align: center; margin: 10px">
            <img class="img-thumbnail" id="output_image" />
        </p>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="form-group {{ $errors->has('detail') ? 'has-danger' : '' }}">
            {!! Form::label('detail', 'Detail', ['class' => 'control-label']) !!} {!! Form::textarea('detail', old('detail'), ['rows'
            => '6', 'placeholder' => 'Details', 'class' => 'form-control ' . ($errors->has('detail') ? 'form-control-danger'
            : '')]) !!} @if ($errors->has('detail'))
            <div class="form-control-feedback">
                {{ $errors->first('detail') }}
            </div>
            @endif
        </div>
    </div>
</div>
<br>
<br>
<p style="text-align: center">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
</p>
{!! Form::close() !!}