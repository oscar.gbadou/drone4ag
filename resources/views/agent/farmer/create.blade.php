@extends('layouts.app') 

@section('content')
     @include('agent.farmer.shared.form', ['url' => ['agent.farmers.store', $farmer], 'method' => 'post'])
@endsection