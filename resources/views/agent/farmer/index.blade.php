@extends('layouts.app') @section('content')
@include('shared.search')
<div class="row">
    <div class="col">
        <h4>Liste des producteurs</h4>
    </div>
    <div class="col">
        <a href="{{ route('agent.farmers.create') }}" class="btn btn-primary" style="float: right;">
            <i class="fas fa-plus"></i> Ajouter un producteur</a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-12">
        <ul class="list-group">
            @foreach($farmers as $farmer)
            <li href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="row">
                    <div style="width: 60px; height: 60px; float: left; margin-right: 15px">
                        <img src="{{ asset($farmer->photo()) }}" alt="" class="img-thumbnail" style="max-height: 60px;">
                    </div>
                    <div style="float: right; width: calc(100% - 85px)">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{ $farmer->first_name }} {{ $farmer->last_name }}</h5>
                            <small>{{ $farmer->created_at }}</small>
                        </div>
                        <p class="mb-1">{{ $farmer->detail }}</p>
                        <small>{{ $farmer->town->name }}, {{ $farmer->district->name }}, {{ $farmer->neighborhood->name }}</small>
                    </div>
                </div>
                <div style="float: right">
                    <a href=" {{ route('agent.farmers.show', ['farmer' => $farmer]) }} " class="btn btn-primary btn-sm">
                        <i class="far fa-eye"></i> Consulter</a>
                    <a href="{{ route('agent.farmers.edit', ['farmer' => $farmer]) }}" class="btn btn-warning btn-sm">
                        <i class="far fa-edit"></i> Modifier</a>
                     @include('agent.farmer.shared.delete', ['url' => ['agent.farmers.destroy', $farmer], 'method' => 'delete'])
                </div>
            </li>
            @endforeach
    </div>
</div>
</div>

@endsection