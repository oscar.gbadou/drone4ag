@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    <h4 class="card-title">Modifier ce producteur</h4>
  </div>
  <div class="card-body">
    @include('agent.farmer.shared.edit', ['url' => ['agent.farmers.update', $farmer], 'method' => 'put'])
  </div>
</div>
@endsection
