{!! Form::model($farmMap, ['route' => $url, 'method' => $method, 'style' => 'display: inline']) !!}
<button title="Supprimer" type="submit" class="btn btn-danger btn-fill btn-sm">
    <i class="nc-icon nc-simple-remove"></i>
</button>
{!! Form::close() !!}
