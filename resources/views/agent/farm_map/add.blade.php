@extends('layouts.app') 

@section('content')
     @include('agent.farm_map.shared.form', ['url' => ['agent.farm-maps.store', $farm->id], 'method' => 'post'])
@endsection