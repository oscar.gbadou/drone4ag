@extends('layouts.app') 

@section('content')
     @include('agent.farm.shared.form', ['url' => ['agent.farms.farmer.store', $farmer->id], 'method' => 'post'])
@endsection