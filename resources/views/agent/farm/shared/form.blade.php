{!! Form::model($farm, ['route' => $url, 'method' => $method, 'files' => true]) !!}
<h4 style="text-align: center">Ajouter un champs pour le producteur <b>{{ $farmer->first_name }} {{ $farmer->last_name }}</b></h4>
<br>
<div class="row">
  <div class="col-md-6 col-sm-12">
    <div class="form-group {{ $errors->has('area') ? 'has-danger' : '' }}">
      {!! Form::label('area', 'Superficie (en hectare)', ['class' => 'control-label']) !!}
      {!! Form::number('area', old('first_name'), ['placeholder' => 'Superficie du champ', 'required', 'class' => 'form-control ' . ($errors->has('area') ? 'form-control-danger' : '')]) !!}
      @if ($errors->has('area'))
      <div class="form-control-feedback">
        {{ $errors->first('area') }}
      </div>
      @endif
    </div>
  </div>
  <div class="col-md-6 col-sm-12">
    <div class="form-group {{ $errors->has('cultivation_practiced') ? 'has-danger' : '' }}">
      {!! Form::label('cultivation_practiced', 'Culture pratiquee', ['class' => 'control-label']) !!}
      {!! Form::text('cultivation_practiced', old('cultivation_practiced'), ['placeholder' => 'Culture pratiquee sur ce champ', 'required', 'class' => 'form-control ' . ($errors->has('cultivation_practiced') ? 'form-control-danger' : '')]) !!}
      @if ($errors->has('cultivation_practiced'))
      <div class="form-control-feedback">
        {{ $errors->first('cultivation_practiced') }}
      </div>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-6">
    <div class="form-group {{ $errors->has('lat') ? 'has-danger' : '' }}">
      {!! Form::label('lat', 'Latitude', ['class' => 'control-label']) !!}
      {!! Form::text('lat', old('lat'), ['placeholder' => 'Latitude', 'class' => 'form-control ' . ($errors->has('lat') ? 'form-control-danger' : '')]) !!}
      @if ($errors->has('lat'))
      <div class="form-control-feedback">
        {{ $errors->first('lat') }}
      </div>
      @endif
    </div>
  </div>
  <div class="col-6">
    <div class="form-group {{ $errors->has('lon') ? 'has-danger' : '' }}">
      {!! Form::label('lon', 'Longitude', ['class' => 'control-label']) !!}
      {!! Form::text('lon', old('lon'), ['placeholder' => 'Longitude', 'class' => 'form-control ' . ($errors->has('lon') ? 'form-control-danger' : '')]) !!}
      @if ($errors->has('lon'))
      <div class="form-control-feedback">
        {{ $errors->first('lon') }}
      </div>
      @endif
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <label for="">Ajouter une carte du champ</label>
    <div class="custom-file">
      <input name="farm_map" type="file" class="custom-file-input" id="customFile" accept="image/*" onchange="preview_image(event)">
      <label class="custom-file-label" for="customFile">Choisir ou prendre photo</label>
    </div>
    <p style="text-align: center; margin: 10px">
      <img class="img-thumbnail" id="output_image" />
    </p>
  </div>
</div>
<br><br>
<p style="text-align: center">
   {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
</p>
{!! Form::close() !!}
