@extends('layouts.app') 

@section('content')
     @include('manager.farmer.shared.form', ['url' => ['manager.farmers.store', $farmer], 'method' => 'post'])
@endsection