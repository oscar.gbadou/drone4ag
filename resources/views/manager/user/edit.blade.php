@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    <h4 class="card-title">Modifier un utilisateur</h4>
  </div>
  <div class="card-body">
    @include('manager.user.shared.edit', ['url' => ['manager.users.update', $user], 'method' => 'put'])
  </div>
</div>
@endsection
