{!! Form::model($user, ['route' => $url, 'method' => $method, 'style' => 'display: inline']) !!}
{!! Form::submit('Supprimer', ['class' => 'btn btn-danger btn-fill btn-xs']) !!}
{!! Form::close() !!}
