{!! Form::model($user, ['route' => $url, 'method' => $method]) !!}
<div class="row">
  <div class="col-md-6">
    <div class="form-group {{ $errors->has('name') ? 'has-danger' : '' }}">
      {!! Form::label('name', 'Nom & prenoms', ['class' => 'control-label']) !!}
      {!! Form::text('name', old('name'), ['placeholder' => 'Nom & prenoms', 'required', 'class' => 'form-control ' . ($errors->has('name') ? 'form-control-danger' : '')]) !!}
      @if ($errors->has('name'))
        <div class="form-control-feedback">
          {{ $errors->first('name') }}
        </div>
      @endif
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group {{ $errors->has('email') ? 'has-danger' : '' }}">
      {!! Form::label('email', 'Adresse email', ['class' => 'control-label']) !!}
      {!! Form::email('email', old('email'), ['placeholder' => 'Adresse email', 'required', 'class' => 'form-control ' . ($errors->has('email') ? 'form-control-danger' : '')]) !!}
      @if ($errors->has('email'))
        <div class="form-control-feedback">
          {{ $errors->first('email') }}
        </div>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group {{ $errors->has('role') ? 'has-danger' : '' }}">
      {!! Form::label('role', 'Role', ['class' => 'control-label']) !!}
      {!! Form::select('role', ['ROLE_AGENT' => 'Agent de terrain', 'ROLE_MANAGER' => 'Gestionnaire', 'ROLE_FARMER' => 'Producteur'], 'ROLE_AGENT', ['required', 'class' => 'form-control ' . ($errors->has('role') ? 'form-control-danger' : '')]) !!}
      @if ($errors->has('role'))
        <div class="form-control-feedback">
          {{ $errors->first('role') }}
        </div>
      @endif
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group {{ $errors->has('password') ? 'has-danger' : '' }}">
      {!! Form::label('password', 'Mot de passe', ['class' => 'control-label']) !!}
      {!! Form::password('password', ['required', 'class' => 'form-control']) !!}
      @if ($errors->has('password'))
        <div class="form-control-feedback">
          {{ $errors->first('password') }}
        </div>
      @endif
    </div>
  </div>
</div>



{!! Form::submit('Ajouter', ['class' => 'btn btn-info']) !!}
{!! Form::close() !!}
