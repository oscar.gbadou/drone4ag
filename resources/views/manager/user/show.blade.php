@extends('layouts.app')

@section('content')

<div class="page-header" style="background: url({{asset('img/developer-template-img/banner1.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-wrapper">
          <h2 class="product-title">Mon profil dev</h2>
          <ol class="breadcrumb">
            <li><a href="{{ route('developer.main.index') }}"><i class="ti-home"></i> Accueil</a></li>
            <li class="current">Profil</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="content" style="padding-top: 40px">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 col-xs-12">
        @include('shared.profil-sidebar')
      </div>
      <div class="col-md-8 col-sm-8 col-xs-12">
        @if(!$developer)
        <div style="margin-top: 20px" class="alert alert-warning alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
          Votre profil dev n'est pas encore configuré
        </div>
        <br>
        <p style="text-align: center">
          <a href="{{ route('developer.developer.create') }}" class="btn btn-common log-btn">Mettre a jour mon profil</a>
        </p>
        @else
        <div class="inner-box my-resume">
          <div class="author-resume">
            <div class="dev-picture-profil thumb"
            style="background-image: url({{ asset($developer->getPhotoPath()) }}); background-size: cover;">
            </div>
            <div class="author-info">
                <div class="col-md-10" style="padding-left: 0px">
                  <h3>{{ $developer->name }} @if($developer->age)({{ $developer->age }}@if($developer->sex), {{ $developer->sex }}@endif) @endif</h3>
                </div>
                <div class="col-md-2" style="padding-right: 0px">
                  <a style="float: right; margin-bottom: 0px" href="{{ route('developer.developer.create') }}" class="btn btn-warning btn-sm">EDITER</a>
                </div>
              <div>
                <p class="sub-title">{{ $developer->title }}</p>
                <p><span class="address"><i class="ti-location-pin"></i>{{ $developer->address }}</span> | <span><i class="ti-phone"></i>{{ $developer->phone }}</span></p>
                <div class="social-link">
                  @if($developer->linkedin)
                  <a class="linkedin" target="_blank" data-original-title="Linkedin" href="{{ $developer->linkedin }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-linkedin"></i></a>
                  @endif
                  @if($developer->twitter)
                  <a class="twitter" target="_blank" data-original-title="Twitter" href="{{ $developer->twitter }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-twitter"></i></a>
                  @endif
                  @if($developer->facebook)
                  <a class="facebook" target="_blank" data-original-title="Facebook" href="{{ $developer->facebook }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-facebook"></i></a>
                  @endif
                  @if($developer->email)
                  <a class="google" target="_blank" data-original-title="Mail" href="mailto:{{ $developer->email }}" data-toggle="tooltip" data-placement="top"><i class="fa fa-envelope"></i></a>
                  @endif
                </div>
                <p style="margin-top: 3px">
                  @foreach($developer->getSkills() as $skill)
                  <span class='label label-default'>{{ $skill['name'] }}</span>
                  @endforeach
                </p>
              </div>


            </div>
          </div>

          <div class="about-me item">
            <h3>Vous en quelques mots</h3>
            <p>{{ $developer->description }}</p>
          </div>
          <div class="work-experence item">
            <h3>Work Experience <i class="ti-pencil"></i></h3>
            <h4>UI/UX Designer</h4>
            <h5>Bannana INC.</h5>
            <span class="date">Fab 2017-Present(5year)</span>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero vero, dolores, officia quibusdam architecto sapiente eos voluptas odit ab veniam porro quae possimus itaque, quas! Tempora sequi nobis, atque incidunt!</p>
            <p><a href="#">4 Projects</a></p>
            <br>
            <h4>UI Designer</h4>
            <h5>Whale Creative</h5>
            <span class="date">Fab 2017-Present(2year)</span>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero vero, dolores, officia quibusdam architecto sapiente eos voluptas odit ab veniam porro quae possimus itaque, quas! Tempora sequi nobis, atque incidunt!</p>
            <p><a href="#">4 Projects</a></p>
          </div>
          <div class="education item">
            <h3>Education <i class="ti-pencil"></i></h3>
            <h4>Massachusetts Institute Of Technology</h4>
            <p>Bachelor of Computer Science</p>
            <span class="date">2010-2014</span>
            <br>
            <h4>Massachusetts Institute Of Technology</h4>
            <p>Bachelor of Computer Science</p>
            <span class="date">2010-2014</span>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection
