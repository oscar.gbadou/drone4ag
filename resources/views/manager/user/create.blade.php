@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-header">
    <h4 class="card-title">Ajouter un utilisateur</h4>
  </div>
  <div class="card-body">
    @include('manager.user.shared.form', ['url' => 'manager.users.store', 'method' => 'post'])
  </div>
</div>
@endsection
