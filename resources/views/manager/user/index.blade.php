@extends('layouts.app')

@section('content')
<p><a href="{{ route('manager.users.create') }}" class="btn btn-primary btn-fill" style="margin-right: 10px">Ajouter un utilisateur</a></p>

<div class="card strpied-tabled-with-hover">
  <div class="card-header ">
    <h4 class="card-title">Utilisateurs</h4>
  </div>
  <div class="card-body">
    <table class="table table-bordered">
      <thead>
        <tr><th>ID</th>
          <th>Nom</th>
          <th>Email</th>
          <th>Role</th>
          <th>Action</th>
        </tr></thead>
        <tbody>
          @foreach($users as $user)
          <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role }}</td>
            <td>
              <a href="{{ route('manager.users.edit', ['user' => $user]) }}" class="btn btn-info btn-fill btn-xs" style="margin-right: 10px">Modifier</a>
              @include('manager.user.shared.delete', ['url' => ['manager.users.destroy', $user], 'method' => 'delete'])
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  @endsection
