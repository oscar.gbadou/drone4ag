@extends('layouts.app') @section('content')
<div class="row">
    <div class="col">
        <h4>Liste des producteurs</h4>
    </div>
    <div class="col">
        <a href="{{ route('farmers.create') }}" class="btn btn-success" style="float: right;">
            <i class="fas fa-plus"></i> Ajouter un producteur</a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-12">
        <ul class="list-group">
            @foreach($farmers as $farmer)
            <li href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">{{ $farmer->first_name }} {{ $farmer->last_name }}</h5>
                    <small>{{ $farmer->created_at }}</small>
                </div>
                <p class="mb-1">{{ $farmer->detail }}</p>
                <small>{{ $farmer->town }} ({{ $farmer->village }})</small>
                <br>
                <div style="float: right">
                    <a href=" {{ route('farmers.show', ['farmer' => $farmer]) }} " class="btn btn-primary btn-sm">
                        <i class="far fa-eye"></i> Consulter</a>
                    <a href="" class="btn btn-warning btn-sm">
                        <i class="far fa-edit"></i> Modifier</a>
                    <a href="" class="btn btn-danger btn-sm">
                        <i class="far fa-trash-alt"></i> Supprimer</a>
                </div>
            </li>
            @endforeach
    </div>
</div>
</div>

@endsection