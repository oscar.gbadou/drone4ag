@extends('layouts.app') 

@section('content')
     @include('manager.farm.shared.form', ['url' => ['manager.farms.farmer.store', $farmer->id], 'method' => 'post'])
@endsection