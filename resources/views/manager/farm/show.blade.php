@extends('layouts.app') @section('content')
<h4 style="text-align: center">Presentation du producteur <b>{{ $farmer->first_name }} {{ $farmer->last_name }}</b></h4>
<br>
<div class="row">
    <div class="col-md-4 col-sm-3">
        <p style="text-align: center">
            <img src="{{ asset($farmer->photo()) }}" alt="" class="img-thumbnail" style="max-height: 250px;">
        </p>
    </div>
    <div class="col-md-8 col-sm-9">
        <p>
            <b>Nom:</b> {{ $farmer->last_name }} </p>
        <p>
            <b>Prenom:</b> {{ $farmer->first_name }} </p>
        <p>
            <b>Commune:</b> {{ $farmer->town }} </p>
        <p>
            <b>Village:</b> {{ $farmer->village }} </p>
        <p>
            <b>Detail:</b> {{ $farmer->detail }} </p>
    </div>
</div>
<br>
<p class="text-secondary">Les champs de <b>{{ $farmer->first_name }} {{ $farmer->last_name }}</b></p>
<p><a href="#" class="btn btn-outline-danger"><i class="fas fa-plus"></i> Ajouter un champ</a></p>
@if(count($farmer->farms) > 0)
@else
<p>Aucun champ enregistrer pour ce producteur.</p>
@endif
@endsection