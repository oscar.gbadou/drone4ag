@extends('layouts.app') 

@section('content')
     @include('manager.farm_map.shared.form', ['url' => ['manager.farm-maps.store', $farm->id], 'method' => 'post'])
@endsection