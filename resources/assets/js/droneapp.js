$(document).ready(function(){
    
    $('.selectpicker').selectpicker({
        size: 5
    });

    $('#customFile').change(function(e){
        preview_image(e);
    });

    $('#town_id').change(function () {
        districts(this.value, 'district_id')
    });

    $('#district_id').change(function () {
        neighborhoods(this.value, 'neighborhood_id')
    });
});

function preview_image(event) {
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('output_image');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
    $('#output_image').show();
}

function districts(town_id, district_element_id) {
    $.ajax({
        url: "/ws/districts",
        data: { id: town_id },
        type: "GET",
        success: function (data) {
            var len = data.length;
            var codeHTML = '';
            for (var i = 0; i < len; i++) {
                codeHTML += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            if ($('#' + district_element_id).attr('class') === 'form-control selectpicker') {
                $('#' + district_element_id).selectpicker('destroy');
            }

            $('#' + district_element_id).html(codeHTML);
            $('#' + district_element_id).attr('class', 'form-control selectpicker');
            $('#' + district_element_id).attr('title', 'Selectionner un arrondissement');
            $('#' + district_element_id).attr('data-live-search', 'true');
            $('.selectpicker').selectpicker({
                style: 'btn-outline-secondary',
                size: 5
            })

        },
        error: function () {
            alert('Impossible de contacter le serveur')
        }
    });

}

function neighborhoods(district_id, neighborhood_element_id) {
    $.ajax({
        url: "/ws/neighborhoods",
        data: { id: district_id },
        type: "GET",
        success: function (data) {
            var len = data.length;
            var codeHTML = '';
            for (var i = 0; i < len; i++) {
                codeHTML += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            if ($('#' + neighborhood_element_id).attr('class') === 'form-control selectpicker') {
                $('#' + neighborhood_element_id).selectpicker('destroy');
            }

            $('#' + neighborhood_element_id).html(codeHTML);
            $('#' + neighborhood_element_id).attr('class', 'form-control selectpicker');
            $('#' + neighborhood_element_id).attr('title', 'Selectionner un village ou quartier');
            $('#' + neighborhood_element_id).attr('data-live-search', 'true');
            $('.selectpicker').selectpicker({
                style: 'btn-outline-secondary',
                size: 5
            })

        },
        error: function () {
            alert('Impossible de contacter le serveur')
        }
    });

}