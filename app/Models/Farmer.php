<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Farmer extends Model
{
    protected $fillable = [
        'first_name', 
        'last_name', 
        'detail', 
        'photo_path',
        'town_id',
        'district_id',
        'neighborhood_id',
        'household_code',
        'responder_code',
        'household_head_name',
        'phone',
        'locality_native',
        'household_type',
        'rice_farmer_number',
        'user_id',
    ];

    public function photo()
    {
        return ($this->photo_path) ? 'storage/' . substr($this->photo_path, 7, strlen($this->photo_path)) : null;
    }

    public function farms()
    {
        return $this->hasMany('App\Models\Farm');
    }

    public function town()
    {
        return $this->belongsTo('App\Models\Town');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\District');
    }

    public function neighborhood()
    {
        return $this->belongsTo('App\Models\Neighborhood');
    }

}
