<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FarmMap extends Model
{
    protected $fillable = [
        'path', 'farm_id', 'comment', 'user_id'
    ];

    public function map()
    {
        return ($this->path) ? 'storage/' . substr($this->path, 7, strlen($this->path)) : null;
    }

    public function farm()
    {
        return $this->belongsTo('App\Models\Farm');
    }
}
