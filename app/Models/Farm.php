<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    protected $fillable = [
        'area', 
        'cultivation_practiced', 
        'farmer_id',
        'lat',
        'lon',
    ];

    public function farmer()
    {
        return $this->belongsTo('App\Models\Farmer');
    }

    public function farmMaps()
    {
        return $this->hasMany('App\Models\FarmMap');
    }
}
