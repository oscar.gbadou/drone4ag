<?php

namespace App\Utils;

class Utils
{

    const BJ_DECOUPAGE_API = 'https://bj-decoupage-territorial.herokuapp.com/api/v1/';
    const SMS_WS_BASE_URL = 'https://westernsms.com/index.php?app=ws';

    /**
     * [registerTransaction description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public static function makeRequest($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::BJ_DECOUPAGE_API . $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $returnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($returnCode == 200) {
            return json_decode($response, true);
        } else {
            return false;
        }

    }

}
