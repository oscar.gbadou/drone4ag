<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Town;
use App\Models\District;
use App\Models\Farmer;
use App\Models\Neighborhood;
use Auth;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        if(Auth::user()->role == 'ROLE_MANAGER'){
            return redirect()->route('manager.farmers.index');
        }elseif(Auth::user()->role == 'ROLE_AGENT'){
            return redirect()->route('agent.farmers.index');
        }else{
            return redirect()->route('farmer.farmers.index');
        }
    }

    public function search(Request $request)
    {
        $qb = Farmer::orderBy('farmers.created_at', 'desc');

        if($request->input('town_id')){
            $qb->where('town_id', $request->input('town_id'));
        }
        if($request->input('district_id')){
            $qb->where('district_id', $request->input('district_id'));
        }
        if($request->input('neighborhood_id')){
            $qb->where('neighborhood_id', $request->input('neighborhood_id'));
        }
        if($request->input('culture')){
            $qb->join('farms', 'farmers.id', '=', 'farms.farmer_id')
            ->where('farms.cultivation_practiced', 'like', '%'.$request->input('culture').'%');
        }

        $farmers = $qb->get();

        $towns = Town::orderBy('name', 'asc')->pluck('name', 'id');

        $user = Auth::user();
        if($user->role == 'ROLE_MANAGER'){
            return view('manager.farmer.index', compact('farmers', 'towns'));
        }elseif($user->role == 'ROLE_AGENT'){
            return view('agent.farmer.index', compact('farmers', 'towns'));
        }else{
            return view('farmer.farmer.index', compact('farmers', 'towns'));
        }
    }

    public function districts(Request $request)
    {
        $districts = District::where('town_id', $request->input('id'))
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($districts);
    }

    public function neighborhoods(Request $request)
    {
        $neighborhoods = Neighborhood::where('district_id', $request->input('id'))
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($neighborhoods);
    }
}
