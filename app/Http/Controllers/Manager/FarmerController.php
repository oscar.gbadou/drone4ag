<?php

namespace App\Http\Controllers\Manager;

use App\Models\Farmer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Town;
use Carbon\Carbon;
use Auth;

class FarmerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmers = Farmer::latest()->get();
        $towns = Town::orderBy('name', 'asc')->pluck('name', 'id');
        return view('manager.farmer.index', compact('farmers', 'towns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $farmer = new Farmer;
        $towns = Town::orderBy('name', 'asc')->pluck('name', 'id');
        return view('manager.farmer.create', compact('farmer', 'towns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $now = Carbon::now();
        $data = $request->all();

        $data['locality_native'] = ($request->input('locality_native'))? true : false;
        $data['user_id'] = Auth::user()->id;

        if ($request->file('photo')) {
            $data['photo_path'] = $request->file('photo')->storeAs(
                'public/farmers', $now->timestamp . '.' . $request->file('photo')->extension()
            );
        }

        $farmer = new Farmer($data);
        if ($farmer->save()) {
            $request->session()->flash('success', 'Producteur ajoute avec succes.');
            return redirect()->route('manager.farmers.index');
        } else {
            $request->session()->flash('error', 'Echec d\'ajout du producteur.');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $farmer = Farmer::findOrFail($id);
        return view('manager.farmer.show', compact('farmer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $farmer = Farmer::findOrFail($id);
        $towns = Town::orderBy('name', 'asc')->pluck('name', 'id');
        return view('manager.farmer.edit', compact('farmer', 'towns'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $farmer = Farmer::findOrFail($id);
        $now = Carbon::now();
        
        $data = $request->all();
        $data['locality_native'] = ($request->input('locality_native')) ? true : false;

        if ($request->file('photo')) {
            $data['photo_path'] = $request->file('photo')->storeAs(
                'public/farmers', $now->timestamp . '.' . $request->file('photo')->extension()
            );
        }

        if ($farmer->update($data)) {
            $request->session()->flash('success', 'Producteur mise a jour avec succes.');
            return redirect()->route('manager.farmers.index');
        } else {
            $request->session()->flash('error', 'Echec de mise a jour du producteur.');
            return back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $farmer = Farmer::findOrFail($id);
        if ($farmer->delete()) {
            $request->session()->flash('success', 'Producteur supprime');
            return back();
        } else {
            $request->session()->flash('error', 'Producteur non supprime');
            return back();
        }

    }
}
