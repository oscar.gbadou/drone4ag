<?php

namespace App\Http\Controllers\Manager;

use App\Models\Farm;
use App\Models\Farmer;
use App\Models\FarmMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;


class FarmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function createForOneFarmer($farmer_id)
    {
        $farmer = Farmer::findOrFail($farmer_id);
        $farm = new Farm;
        return view('manager.farm.create', compact('farmer', 'farm'));
    }

    public function storeForOneFarmer(Request $request, $farmer_id)
    {
        $farmer = Farmer::findOrFail($farmer_id);

        $now = Carbon::now();
        $data = $request->all();
        $data['farmer_id'] = $farmer->id;
        $farm = new Farm($data);

        if ($farm->save()) {
            if ($request->file('farm_map')) {
                $farmMap = new FarmMap([
                    'path' => $request->file('farm_map')->storeAs(
                        'public/farms', $now->timestamp . '_' . $farmer->id . '.' . $request->file('farm_map')->extension()
                    ),
                    'farm_id' => $farm->id,
                    'user_id' => Auth::user()->id,
                    'comment' => ''
                ]);
                $farmMap->save();
            }
            $request->session()->flash('success', 'Champ ajoute avec succes.');
            return redirect()->route('manager.farmers.show', ['farmer' => $farmer]);
        }
        $request->session()->flash('error', 'Echec d\'ajout du champ.');
        return redirect()->route('manager.farmers.show', ['farmer' => $farmer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function show(Farm $farm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function edit(Farm $farm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Farm $farm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Farm $farm)
    {
        //
    }
}
