<?php

namespace App\Http\Controllers\Manager;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::latest()->get();
      return view('manager.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $user = new User;
      return view('manager.user.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = User::create([
        'email' => $request->input('email'),
        'name' => $request->input('name'),
        'role' => $request->input('role'),
        'password' => bcrypt($request->input('password')),
      ]);
      if($user){
        $request->session()->flash('success', 'Utilisateur ajoute avec succes');
        return back();
      }else{
        $request->session()->flash('error', 'Utilisateur non ajoute');
        return back();
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('manager.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
      if ($user->update($request->all())) {
        $request->session()->flash('success', 'Utilisateur modifie avec succes');
      } else {
        $request->session()->flash('error', 'Utilisateur non modifie');
      }
      return redirect()->route('manager.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
      if($user->delete()){
        $request->session()->flash('success', 'Utilisateur supprime');
        return back();
      }else{
        $request->session()->flash('error', 'Utilisateur non supprime');
        return back();
      }
    }
}
