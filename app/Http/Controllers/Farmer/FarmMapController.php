<?php

namespace App\Http\Controllers\Farmer;

use App\Models\Farm;
use App\Models\FarmMap;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FarmMapController extends Controller
{
    public function add($id)
    {
        $farm = Farm::findOrFail($id);
        $farmMap = new FarmMap;
        return view('farmer.farm_map.add', compact('farm', 'farmMap'));
    }

    public function store(Request $request, $id)
    {
        $farm = Farm::findOrFail($id);
        $now = Carbon::now();
        $data = $request->all();
        $data['farm_id'] = $farm->id;
        $data['user_id'] = Auth::user()->id;
        $farmMap = new FarmMap([
            'path' => $request->file('farm_map')->storeAs(
                'public/farms', $now->timestamp . '_' . $farm->farmer->id . '.' . $request->file('farm_map')->extension()
            ),
            'farm_id' => $farm->id,
            'user_id' => Auth::user()->id,
            'comment' => $request->input('comment'),

        ]);

        if ($farmMap->save()) {
            $request->session()->flash('success', 'Carte ajoutee avec succes.');
            return redirect()->route('farmer.farmers.show', ['farmer' => $farm->farmer]);
        } else {
            $request->session()->flash('error', 'Carte non ajoutee.');
            return redirect()->route('farmer.farmers.show', ['farmer' => $farm->farmer]);
        }

    }
}
