/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 38);
/******/ })
/************************************************************************/
/******/ ({

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(39);


/***/ }),

/***/ 39:
/***/ (function(module, exports) {

$(document).ready(function () {

    $('.selectpicker').selectpicker({
        size: 5
    });

    $('#customFile').change(function (e) {
        preview_image(e);
    });

    $('#town_id').change(function () {
        districts(this.value, 'district_id');
    });

    $('#district_id').change(function () {
        neighborhoods(this.value, 'neighborhood_id');
    });
});

function preview_image(event) {
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('output_image');
        output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
    $('#output_image').show();
}

function districts(town_id, district_element_id) {
    $.ajax({
        url: "/ws/districts",
        data: { id: town_id },
        type: "GET",
        success: function success(data) {
            var len = data.length;
            var codeHTML = '';
            for (var i = 0; i < len; i++) {
                codeHTML += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            if ($('#' + district_element_id).attr('class') === 'form-control selectpicker') {
                $('#' + district_element_id).selectpicker('destroy');
            }

            $('#' + district_element_id).html(codeHTML);
            $('#' + district_element_id).attr('class', 'form-control selectpicker');
            $('#' + district_element_id).attr('title', 'Selectionner un arrondissement');
            $('#' + district_element_id).attr('data-live-search', 'true');
            $('.selectpicker').selectpicker({
                style: 'btn-outline-secondary',
                size: 5
            });
        },
        error: function error() {
            alert('Impossible de contacter le serveur');
        }
    });
}

function neighborhoods(district_id, neighborhood_element_id) {
    $.ajax({
        url: "/ws/neighborhoods",
        data: { id: district_id },
        type: "GET",
        success: function success(data) {
            var len = data.length;
            var codeHTML = '';
            for (var i = 0; i < len; i++) {
                codeHTML += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            if ($('#' + neighborhood_element_id).attr('class') === 'form-control selectpicker') {
                $('#' + neighborhood_element_id).selectpicker('destroy');
            }

            $('#' + neighborhood_element_id).html(codeHTML);
            $('#' + neighborhood_element_id).attr('class', 'form-control selectpicker');
            $('#' + neighborhood_element_id).attr('title', 'Selectionner un village ou quartier');
            $('#' + neighborhood_element_id).attr('data-live-search', 'true');
            $('.selectpicker').selectpicker({
                style: 'btn-outline-secondary',
                size: 5
            });
        },
        error: function error() {
            alert('Impossible de contacter le serveur');
        }
    });
}

/***/ })

/******/ });