<?php

use Illuminate\Database\Seeder;
use App\Models\Neighborhood;
use App\Models\District;
use App\Models\Town;


class ParakouNeighborhoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            '1ER ARRONDISSEMENT' => [
                'ALAGA',
                'ALBARIKA',
                'BAKINKOURA',
                'BAKPEROU',
                'BANIKANNI-DOUWEROU',
                'BEYAROU',
                'BOSSO-CAMPS-PEULHS',
                'BOUNDAROU',
                'CAMP-ADAGBE',
                'DAMAGOUROU',
                'DEPOT',
                'GAANON',
                'GOUNIN',
                'KABASSIRA',
                'KADERA',
                'KPEBIE',
                'KPEROU-GUERA',
                'MADINA',
                'MONNON',
                'OUEZE',
                'SAWARAROU',
                'SINAGOUROU',
                'SOUROU',
                'THIAN',
                'TITIROU',
                'TOUROU-DISPENSAIRE',
                'TOUROU-PALAIS-ROYAL',
                'WOROU-TOKOROU',
                'ZAZIRA'
            ],
            '2EME ARRONDISSEMENT' => [
                'AGBA AGBA',
                'ASSAGBININ BAKA',
                'BAKOUNOUROU',
                'BANIKANNI',
                'BANIKANNI-ENI',
                'BANIKANNI-MADJATOM',
                'BAPARAPE',
                'GOROMOSSO',
                'KOROBOROROU',
                'KOROBOROROU-PEULH',
                'LADJIFARANI',
                'LADJIFARANI-PETIT PERE',
                'LEMANDA',
                'NIMA-SOKOUNON',
                'ROSE-CROIX BAH MORA',
                'WOUBEKOU-GAH',
                'ZONGO-ZENON'
            ],
            '3EME ARRONDISSEMENT' => [
                'AMANWIGNON',
                'DOKPAROU',
                'GAH',
                'GANOU',
                'GBIRA',
                'GUEMA',
                'NIKKIKPEROU',
                'SWINROU-KPASSAGAMBOU',
                'TRANZA',
                'WANSIROU',
                'WORE',
                'ZONGO'
            ]
        ];

        $parakou = Town::where('name', 'PARAKOU')->first();
        $parakouDistricts = District::where('town_id', $parakou->id)->get();

        foreach($parakouDistricts as $d){
            foreach($data[$d->name] as $n){
                $oldn = Neighborhood::where([
                    ['name', '=', $n],
                    ['town_id', '=', $parakou->id]
                ])->first();
                
                if(!$oldn){
                    Neighborhood::create([
                        'name' => $n,
                        'town_id' => $parakou->id,
                        'district_id' => $d->id,
                    ]);
                }
            }
        }
    }
}
