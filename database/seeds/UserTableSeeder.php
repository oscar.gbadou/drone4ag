<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@drone4ag.com',
            'name' => 'Gestionnaire',
            'role' => 'ROLE_GESTIONNAIRE',
            'password' => bcrypt('@admin'),
        ]);
    }
}
