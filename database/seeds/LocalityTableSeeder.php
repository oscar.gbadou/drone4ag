<?php

use App\Models\Department;
use App\Models\Town;
use App\Models\District;
use App\Utils\Utils;
use Illuminate\Database\Seeder;

class LocalityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = Utils::makeRequest('departments');
        foreach ($departments['departments'] as $d) {
            $department = Department::create($d);
            $towns = Utils::makeRequest('departments/' . $department->name . '/towns');
            foreach($towns['towns'] as $t){
                $town = Town::create([
                    'name' => $t['name'],
                    'department_id' => $department->id
                ]);
                $districts = Utils::makeRequest('towns/'.$town->name.'/districts');
                foreach($districts['districts'] as $dt){
                    $district = District::create([
                        'name' => $dt['name'],
                        'town_id' => $town->id
                    ]);
                }
            }
        }
    }
}
