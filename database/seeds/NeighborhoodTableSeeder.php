<?php

use App\Models\District;
use App\Models\Neighborhood;
use App\Utils\Utils;
use Illuminate\Database\Seeder;

class NeighborhoodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts = District::all();
        foreach ($districts as $district) {
            $neighborhoods = Utils::makeRequest('districts/' . $district->name . '/neighborhoods');
            if ($neighborhoods && count($neighborhoods) > 0) {
                foreach ($neighborhoods['neighborhoods'] as $n) {
                    $neighborhood = Neighborhood::create([
                        'name' => $n['name'],
                        'town_id' => $district->town_id,
                        'district_id' => $district->id,
                    ]);
                }
            }
        }
    }
}
