<?php

use Illuminate\Database\Seeder;
use App\Models\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
            'email' => 'admin@drone4ag.com',
            'name' => 'Gestionnaire',
            'role' => 'ROLE_GESTIONNAIRE',
            'password' => bcrypt('@admin'),
        ]);

    }
}
